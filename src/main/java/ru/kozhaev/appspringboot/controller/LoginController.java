package ru.kozhaev.appspringboot.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@Controller
public class LoginController {

    @GetMapping(value = "login")
    public String loginPage(Model model) {
        model.addAttribute("title", "Login page");
        return "pages/login";
    }

}
