# CRUD - веб-приложение "Менеджер пользователей"

1. По-умолчанию создаются пользователи с правами администратора и без.
2. Есть возможность создавать, удалять, редактировать и просматривать список пользователей для администратора.
3. Обычные пользователи могут авторизоваться под своими учетными данными - email и пароль.

## Стек технологий 
Java 11, Spring (Boot, MVC, JPA, Security) MySQL, Lombok, Maven, Thymeleaf, Bootstrap, JavaScript (Fetch)

## Контакты

[Отправить письмо](mailto:derecov+crud_project_users@gmail.com)

## License

[MIT](https://choosealicense.com/licenses/mit/)